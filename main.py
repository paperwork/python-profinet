import argparse


import profinet.dcp
import profinet.rpc
import profinet.ap

from profinet.util import *
from profinet.protocol import *


parser = argparse.ArgumentParser()
parser.add_argument("-i", required=True,
                    help="use INTERFACE", metavar="INTERFACE")
parser.add_argument("action", choices=("discover", "get-param", "set-param", "read", "read-inm0-filter", "read-inm0", "read-inm1", "write-inm1", "readap"))
parser.add_argument("target", nargs='?', help="MAC address of the device")
parser.add_argument("param",  nargs='?', help="parameter to read/write")
parser.add_argument("value",  nargs='?', help="value to write")
parser.add_argument("additional1",  nargs='?', help="additional parameters")
parser.add_argument("additional2",  nargs='?', help="additional parameters")

args = parser.parse_args()

s = 0
src = 0
if args.action != "readap":
    s = ethernet_socket(args.i, 3)
    src = get_mac(args.i)

if args.action == "discover":
    profinet.dcp.send_discover(s, src)
    profinet.dcp.read_response(s, src, debug=True)
elif args.action == "get-param":
    profinet.dcp.get_param(s, src, args.target, args.param)
elif args.action == "set-param":
    profinet.dcp.set_param(s, src, args.target, args.param, args.value)
elif args.action == "readap":
    import struct
    con = profinet.rpc.RPCCon(None)
    con.init(ip=args.target, deviceId=0x106, vendorId=0x14d)
    print("Connecting to device ...")
    con.connect(bytes([0x3c, 0x49, 0x37, 0x07, 0x14, 0x7e]))
    """
    block = PNBlockHeader(0x7000, 16, 1, 0)
    data = APRDReq(bytes(block), 0x01, 0x01, 0xAFFE, int(args.param), int(args.value), int(args.additional1), int(args.additional2))
    #data = bytes(block) + struct.pack(">BBHHHIH", 0x01, 0x01, 0xAFFE, int(args.param), int(args.value), int(args.additional1), int(args.additional2))

    con.write(api=0, slot=int(args.param), subslot=int(args.value), idx=0xc9, data=bytes(data))

    errorCode1 = 194
    rdata = 0
    while errorCode1 == 194:
        rdata, status = con.read(api=0, slot=int(args.param), subslot=int(args.value), idx=0xc9)
        print("Status=0x%08x" % status)
        errorCode1 = (status >> 16) & 0xFF

    print(rdata.payload)
    ap_data = APRDResp(rdata.payload)
    print("Status=%d Data Length=%d" % (ap_data.status, ap_data.length))
    print(ap_data.payload)
    """
    print(profinet.ap.ap_parameter_read(con, int(args.param), int(args.value), int(args.additional1), int(args.additional2)))

    con.release()

elif args.action.startswith("read") or args.action.startswith("write"):
    print("Getting station info ...")
    info = profinet.rpc.get_station_info(s, src, args.target)
    con = profinet.rpc.RPCCon(info)

    print("Connecting to device ...")
    con.connect(src)

    if args.action == "read":
        data, status = con.read(api=int(args.param), slot=int(args.value), subslot=int(args.additional1), idx=int(args.additional2, 16))
        print(data.payload)

    if args.action[5:] == "inm0-filter":

        data = con.read_inm0filter()
        for api in data.keys():
            for slot_number, (module_ident_number, subslots) in data[api].items():
                print("Slot %d has module 0x%04X" % (slot_number, module_ident_number))
                for subslot_number, submodule_ident_number in subslots.items():
                    print("  Subslot %d has submodule 0x%04X" % (subslot_number, submodule_ident_number))

    elif args.action[5:] == "inm0":
        data, status = con.read(api=int(args.param), slot=int(args.value), subslot=int(args.additional1), idx=PNInM0.IDX)
        inm0 = PNInM0(data.payload)
        print(inm0)

    elif args.action[5:] == "inm1":
        data, status = con.read(api=int(args.param), slot=int(args.value), subslot=int(args.additional1), idx=PNInM1.IDX)
        inm1 = PNInM1(data.payload)
        print(inm1)

    elif args.action[6:] == "inm1":
        api = int(args.param)
        slot = int(args.value)
        subslot = int(args.additional1)
        data, status = con.read(api, slot, subslot, PNInM1.IDX)
        inm1 = PNInM1(data.payload)
        inm1 = PNInM1(inm1.block_header, bytes(args.additional2, "utf-8"), inm1.im_tag_location)
        con.write(api, slot, subslot, PNInM1.IDX, inm1)


