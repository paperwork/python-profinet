from .util import *
from .protocol import *

import struct

APRDReq = make_packet("APRDReq", (
        ("block_header", "6s"),
        ("service_request", "B"),
        ("modifier", "B"),
        ("request_reference", "H"),
        ("slot", "H"),
        ("subslot", "H"),
        ("parameter_id", "I"),
        ("parameter_instance", "H")
), payload=False)

APRDResp = make_packet("APRDResp", (
        ("block_header", "6s"),
        ("service_request", "B"),
        ("modifier", "B"),
        ("request_reference", "H"),
        ("status", "H"),
        ("length", "H")
))

def ap_parameter_read(rpc_conn, slot, subslot, parameter_id, parameter_instance):
    block = PNBlockHeader(0x7000, 16, 1, 0)
    data = APRDReq(bytes(block), 0x01, 0x01, 0xAFFE, slot, subslot, parameter_id, parameter_instance)

    rpc_conn.write(api=0, slot=slot, subslot=subslot, idx=0xc9, data=bytes(data))

    errorCode1 = 194
    rdata = 0
    while errorCode1 == 194:
        rdata, status = rpc_conn.read(api=0, slot=slot, subslot=subslot, idx=0xc9)
        print("Status=0x%08x" % status)
        errorCode1 = (status >> 16) & 0xFF

    print(rdata.payload)
    ap_data = APRDResp(rdata.payload)
    print("Status=%d Data Length=%d" % (ap_data.status, ap_data.length))
    print(ap_data.payload)
    return ap_data.payload

